# Created by Joseph M. Gordon
# October 22, 2014

import sys

class PalindromeMaker:

    def _add(self, an_int):
        stringify = str(an_int)
        reverse_it = stringify[::-1]
        reversed_int = int(reverse_it)
        int_result = an_int + reversed_int
        return int_result

    def _palindrome_checker(self, int_result):
        result_string = str(int_result)
        result_reversed = result_string[::-1]
        is_palindrome = result_reversed == result_string       
        return is_palindrome#

    def run_palindrome_maker(self, start_int):
        an_int = start_int
        is_palindrome = False
        counter = 0
        while is_palindrome == False:
            counter += 1
            an_int = self._add(an_int)
            is_palindrome = self._palindrome_checker(an_int)
            if counter == 99 and is_palindrome == False:
                print('No palindrome after %d additions' %counter)
                return None
        print('%d %d'%(counter, an_int))

def handler(func, CLArg):
    file = open(CLArg, 'r')
    for line in file:
        start_int = int(line)
        func(start_int)
    file.close()
    return 0

if __name__ == '__main__':
    MakeIt = PalindromeMaker()
    handler(MakeIt.run_palindrome_maker, sys.argv[1])
    

