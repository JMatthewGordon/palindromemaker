This program creates a palindrome number starting with a "seed" integer. Begining with the
initial number the input is reversed then added to its unreversed self. The result is checked
for "palindromeness". If true the process terminates (if there are more test cases we proceed
to the next case). If the result is not a palindrome number we repeat the process. If a 
palindrome is not found after 99 interations we terminate the process (or if there are more
test cases we proceed to the next case).
